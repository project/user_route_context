<?php

namespace Drupal\Tests\user_route_context\Kernel\ContextProvider;

use Drupal\Core\Routing\RouteMatch;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user_route_context\ContextProvider\UserRouteContext;

/**
 * Tests the user object from route context.
 *
 * @group user_route_context
 */
class UserRouteContextTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'user_route_context',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
  }

  /**
   * @covers UserRouteContext::getAvailableContexts
   */
  public function testGetAvailableContexts() {
    $context_repository = $this->container->get('context.repository');

    // Test user_route_context.user_route_context:user exists.
    $contexts = $context_repository->getAvailableContexts();
    $this->assertArrayHasKey('@user_route_context.user_route_context:user', $contexts);
    $this->assertSame('entity:user', $contexts['@user_route_context.user_route_context:user']->getContextDefinition()
      ->getDataType());
  }

  /**
   * @covers UserRouteContext::getRuntimeContexts
   */
  public function testGetRuntimeContexts() {
    $user = $this->createUser();

    // Create RouteMatch from user entity.
    $url = $user->toUrl();
    $route_provider = \Drupal::service('router.route_provider');
    $route = $route_provider->getRouteByName($url->getRouteName());
    $route_match = new RouteMatch($url->getRouteName(), $route, [
      'user' => $user,
    ]);

    // Initiate UserRouteContext with RouteMatch.
    $provider = new UserRouteContext($route_match);

    $runtime_contexts = $provider->getRuntimeContexts([]);
    $this->assertArrayHasKey('user', $runtime_contexts);
    $this->assertTrue($runtime_contexts['user']->hasContextValue());
  }

}
